import openpyxl as xl
from operator import itemgetter
from openpyxl.styles import PatternFill
from sklearn import linear_model
import pandas as pd
import json 
import statsmodels.api as sm


import mysql.connector

mydb = mysql.connector.connect(
  host="206.189.144.182",
  user="lagoon",
  password="#$LaG00n4rhkbDwef",
  database="lagoon"
)

mycursor = mydb.cursor()

book = xl.Workbook()

book.create_sheet("Data")
book.create_sheet("Note")

book.active = book['Data']
sheet = book['Data']

workbook = xl.load_workbook('data_final.xlsx')
workbook.active

worksheet = workbook['Sheet1'] 
worksheet = workbook.active

lg_south = [];
lg_middle = [];
lg_top = [];

##################################################################################  Sort Data

for i in range(3 , 30758):

    ska = worksheet[f'A{i}']

    cell = worksheet[f'C{i}']
    x = worksheet[f'B{i}']

    if cell.value <= 825000:


        raw = {};
        raw['y'] = cell.value;
        raw['x'] = x.value;
        raw['excel_row'] = i

        if ska.value != None:
            print(ska.value)
            raw['ska'] = ska.value
        else :
            raw['ska'] = "None"

        lg_south.append(raw);


    if cell.value > 825000 and cell.value <= 835000:
    
        raw = {};
        raw['y'] = cell.value;
        raw['x'] = x.value;
        raw['excel_row'] = i

        if ska.value != None:
            raw['ska'] = ska.value
            print(f'{ska.value}')
        else :
            raw['ska'] = "None"

        lg_middle.append(raw);

    if cell.value > 835000 and cell.value <= 865000:
        
        raw = {};
        raw['y'] = cell.value;
        raw['x'] = x.value;
        raw['excel_row'] = i

        if ska.value != None:
            raw['ska'] = ska.value
            print(f'{ska.value}')
        else :
            raw['ska'] = "None"

        lg_top.append(raw);



##################################################################################  Train


############################### south


data_train_south = {"x" : [] , "y" : []}
data_train_south_df = {}
data_x_level_water = []


for i in range(4 , 184):
        cell = worksheet.cell(row=2, column=i)
        data_x_level_water.append(cell.value);


data_train_south_df['level_water'] = data_x_level_water;


for dict_item in lg_south:   
    raw = {};

    data_x = [];
    data_y = []

    data_df = [];

    if dict_item['ska'] != "None":
   
        raw["ska"] = dict_item['ska']
        raw["row_ska"] = dict_item['excel_row']
        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
    
        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)

            data_x.append(cell.value)
            data_df.append(cell.value)


        raw['data'] = data_x            
        data_train_south["x"].append(raw)

        data_train_south_df[dict_item['ska']] = data_df


for dict_item in lg_south:   
    raw = {};
    data_y_df = [];
    data_train_south_df["y"] = []

    if dict_item['ska'] == "None":

        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
        raw["row_y"] = dict_item['excel_row']

        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)
            data_y_df.append(cell.value)

        data_train_south_df["y"] = data_y_df


        if sum(data_train_south_df["y"]) == 0 :
            continue;

        else:
            df = pd.DataFrame(data_train_south_df)

            x = df[['SKA09' , 'SKA10' , 'level_water']]
            y = df['y']


            regr = linear_model.LinearRegression()
            regr.fit(x, y)

            x = sm.add_constant(x)
            
            model = sm.OLS(y, x).fit()
            predictions = model.predict(x) 

            print_model = model.summary()



            print(f" south {dict_item['excel_row']} : {dict_item['x']},{dict_item['y']}")

            sql = "insert into equation_south (x, y, intercept, coefficients_ska09, coefficients_ska10, coefficients_level_water ,r_squared, adj_r_squared, detail_model) values (%s , %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (dict_item['x'] ,  dict_item['y'],  regr.intercept_ , regr.coef_.item(0) , regr.coef_.item(1) , regr.coef_.item(2) , model.rsquared , model.rsquared_adj  , str(print_model))
            mycursor.execute(sql, val)

            mydb.commit()


############################### middle
data_train_middle = {"x" : [] , "y" : []}
data_train_middle_df = {}


data_train_middle_df['level_water'] = data_x_level_water;


for dict_item in lg_middle:   
    raw = {};

    data_x = [];
    data_y = []

    data_df = [];

    if dict_item['ska'] != "None":
   
        raw["ska"] = dict_item['ska']
        raw["row_ska"] = dict_item['excel_row']
        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
    
        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)

            data_x.append(cell.value)
            data_df.append(cell.value)


        raw['data'] = data_x            
        data_train_middle["x"].append(raw)

        data_train_middle_df[dict_item['ska']] = data_df


for dict_item in lg_middle:   
    raw = {};
    data_y_df = [];
    data_train_middle_df["y"] = []

    if dict_item['ska'] == "None":

        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
        raw["row_y"] = dict_item['excel_row']

        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)
            data_y_df.append(cell.value)

        data_train_middle_df["y"] = data_y_df


        if(sum(data_train_middle_df["y"] ) == 0) :
            continue;
        
        else :
            df = pd.DataFrame(data_train_middle_df)

            x = df[['SKA06' , 'SKA07' , 'SKA08' ,'level_water']]
            y = df['y']


            regr = linear_model.LinearRegression()
            regr.fit(x, y)

            x = sm.add_constant(x)
            
            model = sm.OLS(y, x).fit()
            predictions = model.predict(x) 
            print_model = model.summary()

            print(f" middle {dict_item['excel_row']} : {dict_item['x']},{dict_item['y']}")

            sql = "insert into equation_middle (x, y, intercept, coefficients_ska06, coefficients_ska07, coefficients_ska08  ,coefficients_level_water ,r_squared, adj_r_squared, detail_model) values (%s ,  %s , %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (dict_item['x'] ,  dict_item['y'],  regr.intercept_ , regr.coef_.item(0) , regr.coef_.item(1) , regr.coef_.item(2) , regr.coef_.item(3) , model.rsquared , model.rsquared_adj  , str(print_model))
        
            mycursor.execute(sql, val)

            mydb.commit()

############################### top

data_train_top = {"x" : [] , "y" : []}
data_train_top_df = {}


data_train_top_df['level_water'] = data_x_level_water;


for dict_item in lg_top:   
    raw = {};

    data_x = [];
    data_y = []

    data_df = [];

    if dict_item['ska'] != "None":
   
        raw["ska"] = dict_item['ska']
        raw["row_ska"] = dict_item['excel_row']
        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
    
        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)

            data_x.append(cell.value)
            data_df.append(cell.value)


        raw['data'] = data_x            
        data_train_top["x"].append(raw)

        data_train_top_df[dict_item['ska']] = data_df


for dict_item in lg_top:   
    raw = {};
    data_y_df = [];
    data_train_top_df["y"] = []

    if dict_item['ska'] == "None":

        raw["x"] = dict_item['x']
        raw["y"] = dict_item['y']
        raw["row_y"] = dict_item['excel_row']

        for i in range(4 , 184):
            cell = worksheet.cell(row=dict_item['excel_row'], column=i)
            data_y_df.append(cell.value)

        data_train_top_df["y"] = data_y_df

        if sum(data_train_top_df["y"]) == 0 :
            continue;
        else :

            df = pd.DataFrame(data_train_top_df)

            x = df[['SKA01' , 'SKA02' , 'SKA03' , 'SKA04' , 'SKA05'   ,'level_water']]
            y = df['y']


            regr = linear_model.LinearRegression()
            regr.fit(x, y)

            x = sm.add_constant(x)
            
            model = sm.OLS(y, x).fit()
            predictions = model.predict(x) 
            print_model = model.summary()

            print(f" top {dict_item['excel_row']} : {dict_item['x']},{dict_item['y']}")

            sql = "insert into equation_top (x, y, intercept, coefficients_ska01, coefficients_ska02, coefficients_ska03  ,coefficients_ska04 ,coefficients_ska05 , coefficients_level_water ,r_squared, adj_r_squared, detail_model) values (%s , %s ,  %s ,  %s , %s, %s, %s, %s, %s, %s, %s, %s)"
            val = (dict_item['x'] ,  dict_item['y'],  regr.intercept_ , regr.coef_.item(0) , regr.coef_.item(1) , regr.coef_.item(2) , regr.coef_.item(3)  , regr.coef_.item(4)   , regr.coef_.item(5)   , model.rsquared , model.rsquared_adj  , str(print_model))
        
            mycursor.execute(sql, val)

            mydb.commit()





# print(df)

# i = 1;


# ska = 1;
# point = 1;

# for dict_item in lg_south:   
#     for col in range(1 , 185):

#         book.active = book['Data']
#         sheet = book['Data']
#         cell = worksheet.cell(row=dict_item['excel_row'], column=col)
   
    
#         sheet.cell(row=i, column=col).value = cell.value;
#         sheet.cell(row=i ,column=col).fill = PatternFill(patternType='solid',fgColor='42bfff')

#         if( col == 1 and cell.value != ""):

#             book.active = book['Note']
#             sheet = book['Note']

#             sheet.cell(row=1, column=ska).value = dict_item['excel_row'] ;
#             sheet.cell(row=2, column=ska).value = cell.value;
#             ska = ska + 1


#     i = i + 1;



# for dict_item in lg_middle: 
#     for col in range(1 , 185):

#         cell = worksheet.cell(row=dict_item['excel_row'], column=col)
   
#         sheet.cell(row=i, column=col).value = cell.value;
#         sheet.cell(row=i ,column=col).fill = PatternFill(patternType='solid',fgColor='ff8242')

#     i = i + 1;

# for dict_item in lg_top: 
#     for col in range(1 , 185):

#         cell = worksheet.cell(row=dict_item['excel_row'], column=col)
   
#         sheet.cell(row=i, column=col).value = cell.value;
#         sheet.cell(row=i ,column=col).fill = PatternFill(patternType='solid',fgColor='d03248')

#     i = i + 1;

# book.save('sort.xlsx')


# print(len(lg_south));
# print(len(lg_middle));
# print(len(lg_top));


